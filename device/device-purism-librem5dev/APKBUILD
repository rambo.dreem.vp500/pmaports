# Reference: <https://postmarketos.org/devicepkg>
# Contributor: Clayton Craft <clayton@craftyguy.net>
pkgname="device-purism-librem5dev"
pkgdesc="Purism Librem 5 Devkit"
pkgver=1.0
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="postmarketos-base
	linux-purism-librem5
	uboot-tools
	u-boot-librem5
	gpsd
	mesa-git-dri-gallium
	ofono
"
makedepends="devicepkg-dev"
source="
	deviceinfo
	uboot-script.cmd
	modprobe.d_rsi.conf
	00-mesa.sh
	00-kwin.sh
	flash_script.lst
"
subpackages="$pkgname-plasma"
install="$pkgname.post-install"

build() {
	mkimage -A arm64 -C none -O linux -T script -d "$srcdir/uboot-script.cmd" "$srcdir/boot.scr"
	devicepkg_build $startdir $pkgname
}

plasma() {
	install_if="$pkgname kwin"
	install -D -m644 "$srcdir"/00-kwin.sh "$subpkgdir"/etc/profile.d/00-kwin.sh
}

package() {
	install -D -m644 "$srcdir"/boot.scr \
		"$pkgdir"/boot/boot.scr
	install -D -m644 "$srcdir"/modprobe.d_rsi.conf "$pkgdir"/etc/modprobe.d/rsi.conf
	install -D -m644 "$srcdir"/00-mesa.sh "$pkgdir"/etc/profile.d/00-mesa.sh
	install -D -m644 "$srcdir"/flash_script.lst "$pkgdir"/usr/share/uuu/flash_script.lst
	devicepkg_package $startdir $pkgname
}

sha512sums="d073e520a6732b9784d4a79e75dccdf4101f60708c429ca3ff1c12b4b2aaaf27aa2f5f1836c48be98fbc10a1a5fbfd888ba45f4651cca4774187a85f2a8b5a47  deviceinfo
e4bb06e337d3f23abb0328a8e564e905ef571cdbeeefe442cbbd7e715f5a5b656d00679c48161736b3254bcba1ab39a6c99402f67bbde9838c881b1adeb360ed  uboot-script.cmd
9dc018f0de523cbfe5a49cbe831aa30e975a8dd34635197bb52582f072ac356ef2c02223fc794d970380091a69a83a74c3fbe34520190c8536e77f9ea98c7659  modprobe.d_rsi.conf
5fdf45859f2bb154bcfe7c7341de4d5b239ef43d3f865e30456af073a8ee9d6682515c7e1dd52ddbe3969669d60689ba93c08ead32aadcfb164ff72a02b2e1a4  00-mesa.sh
685ec71484a932c59f83f1039d4d18a010a5849342e96d9e15ae8ce64374aac15073f9c51d7d9f270c60e70d2ea15c685b8f528cdc40f991b093b9227cba6a48  00-kwin.sh
e1acf19ac91f012f000ab19165327043ffc4e80575dcc05d6aeb457f6343db7ba962fe658839faeb63b453340ddf6a314ccfab57e55d5b9d763bdb621cf5fd63  flash_script.lst"
